const mysql = require('mysql');
const express = require('express')
const bodyparser = require('body-parser');

var app = express();
app.use(bodyparser.json());
//MYSQL Details
var mysqlConnection = mysql.createConnection({
    host: 'localhost',
    user: 'admin',
    password: '',
    database: 'northwind',
    multipleStatements: true
});
mysqlConnection.connect((err)=>{
    if(!err)
    console.log("Connection Established Successfully");
    else console.log("Connection Failed" + JSON.stringify(err, undefined,2));

});
const port = process.env.port || 3306;
app.listen(port,()=>console.log(`Listening on port' ${port}`));